﻿using SignalSystem.Raisers;
using UnityEditor;
using UnityEngine;

namespace SignalSystem.Editor
{
	[CustomEditor(typeof(Raiser))]
	public class RaiserEditor : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			GUI.enabled = Application.isPlaying;
			Raiser raiser = target as Raiser;

			if (GUILayout.Button("Raise")) raiser.Raise();
		}
	}
}