﻿using SignalSystem;
using UnityEditor;
using UnityEngine;

namespace SignalSystem.Editor
{
	[CustomEditor(typeof(SignalSO))]
	public class SignalEditor : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			GUI.enabled = Application.isPlaying;
			SignalSO signalSo = target as SignalSO;

			if (GUILayout.Button("Raise")) signalSo.Raise();
		}
	}
}