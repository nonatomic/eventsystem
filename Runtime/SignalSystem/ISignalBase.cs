﻿namespace SignalSystem
{
	public interface ISignalBase
	{
		void Raise();
		void RemoveAllListeners();

	}
}