﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace SignalSystem
{
	public class SignalListener : MonoBehaviour
	{
		[TextArea] public string description;
		public SignalSO signalSo;
		public UnityEvent onEvent = new UnityEvent();
	
		private void Awake()
		{
			if (signalSo == null) return;
			
			signalSo.AddListener(OnEvent);
		}

		private void OnEvent()
		{
			onEvent.Invoke();
		}
	}
}