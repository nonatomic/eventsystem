﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SignalSystem
{
	[CreateAssetMenu(menuName = "Signal System/SignalString")]
	public class SignalStringSO : ScriptableObject, ISignalBase, ISignalString
	{
		[SerializeField] private List<UnityAction<string>> _actions = new List<UnityAction<string>>();
		public string value;
		
		public void Raise()
		{
			foreach (UnityAction<string> action in _actions)
			{
				if(action != null && action.Target != null) action.Invoke(value);
			}
		}

		public void AddListener(UnityAction<string> action)
		{
			if(!_actions.Contains(action)) _actions.Add(action);
		}

		public void RemoveListener(UnityAction<string> action)
		{
			_actions.Remove(action);
		}

		public void RemoveAllListeners()
		{
			_actions.Clear();
		}
	}
}