﻿using System.Collections.Generic;
using UnityEngine;

namespace SignalSystem
{
	public class SignalLink : MonoBehaviour
	{
		[SerializeField] private SignalSO input;
		[SerializeField] private SignalSO pass;
		[SerializeField] private SignalSO fail;
		[SerializeReference] private List<ISignalGuard> signalGuards = new List<ISignalGuard>();
		
		private void OnEnable()
		{
			if(input != null) input.AddListener(OnEvent);
		}

		private void OnDisable()
		{
			if(input != null) input.RemoveListener(OnEvent);
		}
		
		private void OnEvent()
		{
			if (signalGuards.Count > 0)
			{
				foreach (var guard in signalGuards)
				{
					if (!guard.CanIParse())
					{
						if(fail != null) fail.Raise();
						return;
					}
				}
			}
			
			if(pass != null) pass.Raise();
		}
	}
}