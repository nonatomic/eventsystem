﻿using UnityEngine.Events;

namespace SignalSystem
{
	public interface ISignalVoid
	{
		void AddListener(UnityAction action);
		void RemoveListener(UnityAction action);
	}
}