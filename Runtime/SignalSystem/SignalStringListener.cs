﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace SignalSystem
{
	[Serializable]
	public class SignalStringEvent : UnityEvent<string>{}
	
	public class SignalStringListener : MonoBehaviour
	{
		[TextArea] public string description;
		public SignalStringSO signalSo;
		public SignalStringEvent onEvent = new SignalStringEvent();
	
		private void Awake()
		{
			if (signalSo == null) return;
			
			signalSo.AddListener(OnEvent);
		}

		private void OnEvent(string value)
		{
			onEvent.Invoke(value);
		}
	}
}