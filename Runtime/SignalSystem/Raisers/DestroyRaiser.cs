﻿namespace SignalSystem.Raisers
{
	public class DestroyRaiser : Raiser
	{
		private void OnDestroy()
		{
			Raise();
		}
	}
}