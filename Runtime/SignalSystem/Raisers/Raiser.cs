﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace SignalSystem.Raisers
{
	public abstract class Raiser : MonoBehaviour
	{
		[TextArea] [SerializeField] private string description;
		[SerializeField] private float delay;
		[SerializeReference] public ISignalBase signal;
		[SerializeField] private UnityEvent onEvent = new UnityEvent();

		public void Raise()
		{
			if(delay == 0f)
			{
				Internal_Raise();
			}
			else
			{
				StartCoroutine(DelayedRaise());
			}
		}
		
		private IEnumerator DelayedRaise()
		{
			yield return new WaitForSeconds(delay);
			Internal_Raise();
		}

		private void Internal_Raise()
		{
			if(signal != null) signal.Raise();
			onEvent.Invoke();
		}
	}
}