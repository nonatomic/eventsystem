﻿using System;

namespace SignalSystem.Raisers
{
	public class MultiRaiser : Raiser
	{
		public bool raiseOnAwake;
		public bool raiseOnStart;
		public bool raiseOnEnable;
		public bool raiseOnDisable;
		public bool raiseOnDestroy;

		private void Awake()
		{
			if (raiseOnAwake) Raise();
		}

		private void Start()
		{
			if(raiseOnStart) Raise();
		}

		private void OnEnable()
		{
			if (raiseOnEnable) Raise();
		}

		private void OnDisable()
		{
			if (raiseOnDisable) Raise();
		}

		private void OnDestroy()
		{
			if (raiseOnDestroy) Raise();
		}
	}
}