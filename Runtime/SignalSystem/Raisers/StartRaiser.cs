﻿namespace SignalSystem.Raisers
{
	public class StartRaiser : Raiser
	{
		private void Start()
		{
			Raise();
		}
	}
}