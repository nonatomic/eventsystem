﻿namespace SignalSystem.Raisers
{
	public class AwakeRaiser : Raiser
	{
		private void Awake()
		{
			Raise();
		}
	}
}