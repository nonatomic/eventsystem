﻿namespace SignalSystem.Raisers
{
	public class EnabledRaiser : Raiser
	{
		private void OnEnable()
		{
			Raise();
		}
	}
}