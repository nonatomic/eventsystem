﻿namespace SignalSystem.Raisers
{
	public class DisableRaiser : Raiser
	{
		private void OnDisable()
		{
			Raise();
		}
	}
}