﻿using System;

namespace SignalSystem
{
	public interface ISignalGuard
	{
		public bool CanIParse();
	}
}