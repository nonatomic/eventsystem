﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SignalSystem
{
	[CreateAssetMenu(menuName = "Signal System/Signal Hub")]
	public class SignalHubSO : ScriptableObject
	{
		private Dictionary<Type, UnityEventBase> _events = new Dictionary<Type, UnityEventBase>();

		public T Get<T>() where T : UnityEventBase, new()
		{
			Type eventType = typeof(T);
			if(_events.ContainsKey(eventType)) return (T)_events[eventType];

			var e = new T();
			_events.Add(eventType, e);
			return (T)e;
		}

		public void Clear()
		{
			foreach(KeyValuePair<Type, UnityEventBase> kvp in _events)
				(kvp.Value).RemoveAllListeners();

			_events.Clear();
		}

		public bool Has<T>() where T : UnityEventBase
		{
			return _events.ContainsKey(typeof(T));
		}

		public int Count { get => _events.Count; }
		
	}
}