﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SignalSystem
{
	[CreateAssetMenu(menuName = "Signal System/Signal")]
	public class SignalSO : ScriptableObject, ISignalBase, ISignalVoid
	{
		[SerializeField] private List<UnityAction> _actions = new List<UnityAction>();
		
		public void Raise()
		{
			foreach (UnityAction action in _actions)
			{
				if(action != null && action.Target != null) action.Invoke();
			}
		}

		public void AddListener(UnityAction action)
		{
			if(!_actions.Contains(action)) _actions.Add(action);
		}

		public void RemoveListener(UnityAction action)
		{
			_actions.Remove(action);
		}

		public void RemoveAllListeners()
		{
			_actions.Clear();
		}
	}
}