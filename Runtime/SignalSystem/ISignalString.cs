﻿using UnityEngine.Events;

namespace SignalSystem
{
	public interface ISignalString
	{
		void AddListener(UnityAction<string> action);
		void RemoveListener(UnityAction<string> action);
	}
}