﻿using SignalSystem;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Events;

namespace Tests.Editor
{
	public class GameEventTests
	{
		[Test]
		public void Event_Is_Raised()
		{
			var signal = ScriptableObject.CreateInstance<SignalSO>();
			var raiseCount = 0;
			UnityAction raiseAction = () => { raiseCount++; };
			
			signal.AddListener(raiseAction);
			signal.Raise();
			
			Assert.AreEqual(1, raiseCount);
		}

		[Test]
		public void Event_Listener_Is_Removed()
		{
			var signal = ScriptableObject.CreateInstance<SignalSO>();
			var raiseCount = 0;
			UnityAction raiseAction = () => { raiseCount++; };

			signal.AddListener(raiseAction);
			signal.Raise();
			signal.RemoveListener(raiseAction);
			signal.Raise();
			
			Assert.AreEqual(1,raiseCount);
		}

		[Test]
		public void All_Listeners_Are_Removed()
		{
			var signal = ScriptableObject.CreateInstance<SignalSO>();
			var raiseCount = 0;
			UnityAction raiseActionOne = () => { raiseCount+= 1; };
			UnityAction raiseActionTwo = () => { raiseCount+= 2; };
			signal.AddListener(raiseActionOne);
			signal.AddListener(raiseActionTwo);
			signal.Raise();
			signal.RemoveAllListeners();
			signal.Raise();
			
			Assert.AreEqual(3,raiseCount);
		}
	}
}