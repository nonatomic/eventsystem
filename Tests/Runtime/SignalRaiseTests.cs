﻿using System.Collections;
using SignalSystem;
using NUnit.Framework;
using SignalSystem.Raisers;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests.Runtime
{
	public class GameEventRaiseTests
	{
		[UnityTest]
		public IEnumerator Does_Raise_Event_On_Start()
		{
			var gameObject = new GameObject();
			var raiser = gameObject.AddComponent<MultiRaiser>();
			var signal = ScriptableObject.CreateInstance<SignalSO>();
			
			var raiseCount = 0;
			signal.AddListener(() => { raiseCount++; });

			raiser.signal = signal;
			raiser.raiseOnStart = true;

			yield return new WaitForSeconds(1);
			
			Assert.AreEqual(1, raiseCount);
			GameObject.Destroy(gameObject);
		}
	}
}